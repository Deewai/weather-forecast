#!/usr/bin/python3
import unittest
from api.api import Api
class TestSum(unittest.TestCase):
    def test_get_location(self):
        api = Api()
        self.assertIsInstance(api.getLocation('lagos'), list)
    def test_get_weather(self):
        api = Api()
        self.assertIsInstance(api.getWeather('lagos'), list)