# -*- coding: cp1252 -*-
#!/usr/bin/python3
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import cairo

class Plot:
    def __init__(self, weatherData, title = ''):
        self.data = weatherData
        self.xAxis = []
        self.yAxis = []
        self.title = title
        self.setValues()

    def setValues(self):
        for elem in self.data:
            day =  elem['LocalTime'][4:6]
            hour = elem['LocalTime'][:2]
            self.xAxis.append("%s(%s)" % (day, hour))
            self.yAxis.append(float(elem['Temperature']))

    def plot(self):
        plt.plot(self.xAxis, self.yAxis)
        print(len(self.xAxis))
        plt.xlabel('Time - Day(Hour)')
        plt.ylabel('Temperature %sC' % (u'\N{DEGREE SIGN}'))
        plt.title('%s Temperature History' % (self.title))
        plt.savefig('./graph')
        self.loadImage()
    def loadImage(self):
        self.ims = cairo.ImageSurface.create_from_png("./graph.png")
        
            
    def onDraw(self, wid, cr):
        cr.set_source_surface(self.ims, 10, 10)
        cr.paint()