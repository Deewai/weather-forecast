This solution assumes all dependencies in the dependencies.txt have been installed (could be done using (pip3 install <package_name>))

The solution is compatible with python3+ and so assumes pip3 is installed
tests can be found in the test directory

entry point of the program is main.py
The Gui Template can be found in the root directory: weather-forecast.glade