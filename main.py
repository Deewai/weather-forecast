# -*- coding: cp1252 -*-
#!/usr/bin/python3
import gi
import json
import requests
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk, GdkPixbuf, Gio
from db.db import Db
from api.api import Api
from graph.graph import Plot


class Builder(Gtk.Builder,Db, Api):
    def __init__(self):
        Gtk.Builder.__init__(self)
        Db.__init__(self)
        self.add_objects_from_file('./weather-forecast.glade', ('window', ''))
        self.window = self.get_object('window')
        self.window.show()
        self.window.set_title('Weather Forecast')
        self.window.set_size_request(500, 500)
        self.setObjects()
        self.setScreens()
        self.menuSignals()
        self.buttonSignals()

    def setObjects(self):
        self.searchMenu = self.get_object('search-menu')
        self.historyMenu = self.get_object('history-menu')
        self.searchButton = self.get_object('search-action')
        self.searchField = self.get_object('location-search')
        self.searchScreen = self.get_object('search-screen')
        self.historyScreen = self.get_object('history-screen')
        self.weatherResult = self.get_object('weather-result')
        self.graphScreen = self.get_object('graph-view')


    def menuSignals(self):
        self.searchMenu.connect('select', self.onSearchMenuPress)
        self.historyMenu.connect('select', self.onHistoryMenuPress)
    
    def buttonSignals(self):
        self.searchButton.connect('clicked', self.onSearchButtonPress)

    def setScreens(self):
        self.grid = Gtk.Grid()
        self.grid.set_column_homogeneous(True)
        self.grid.set_row_homogeneous(True)
        self.historyScreen.add(self.grid)
        self.treeview = Gtk.TreeView()
        select = self.treeview.get_selection()
        select.connect("changed", self.onTreeSelectionChanged)
        self.scrollable_treelist = Gtk.ScrolledWindow()
        self.scrollable_treelist.set_vexpand(True)
        self.grid.attach(self.scrollable_treelist, 0, 0, 8, 10)
        self.scrollable_treelist.add(self.treeview)

    def onSearchMenuPress(self,menu):
        self.historyScreen.hide()
        self.graphScreen.hide()
        self.searchScreen.show()

    def onHistoryMenuPress(self,menu):
        self.searchScreen.hide()
        self.graphScreen.hide()
        self.historyScreen.show()
        self.store = self.storeModel()
        self.store_filter = self.store.filter_new()
        self.treeview.set_model(self.store_filter)
        columns = self.treeview.get_columns()
        for elem in columns:
            self.treeview.remove_column(elem)
        for i, column_title in enumerate(["Title", "Vicinity"]):
            renderer = Gtk.CellRendererText()
            column = Gtk.TreeViewColumn(column_title, renderer, text=i)
            self.treeview.append_column(column)
        self.historyScreen.show_all()

    def onTreeSelectionChanged(self, selection):
        model, treeiter = selection.get_selected()
        if treeiter is not None:
            data = self.get(model[treeiter][0],model[treeiter][1])
            if data is not None:
                self.historyScreen.hide()
                self.searchScreen.hide()
                self.graphScreen.show()
                plot = Plot(data['weather'], data['location']['Title'])
                plot.plot()
                self.graphScreen.connect("draw", plot.onDraw)
                self.window.set_size_request(800,700)

    def onSearchButtonPress(self, button):
        resultBuffer = self.weatherResult.get_buffer()
        resultBuffer.set_text('Loading...')
        query = self.searchField.get_text()
        location = self.getLocation(query)
        if (len(location) > 0):
            weather = self.getWeather(location[0]['Title'])
            self.save(location[0], weather)
            resultBuffer.set_text('')
            response = requests.get(weather[0]['IconLink'])
            input_stream = Gio.MemoryInputStream.new_from_data(response.content, None) 
            pixbuf = GdkPixbuf.Pixbuf.new_from_stream(input_stream, None)
            pixbuf.scale_simple(100, 100, GdkPixbuf.InterpType.BILINEAR)
            resultBuffer.insert_pixbuf(resultBuffer.get_end_iter(), pixbuf)
            resultBuffer.insert(resultBuffer.get_end_iter(),"%s%sC %s\n\n  Humidity: %s\n\n  Day/Time: %s\n\n  Day of the week: %s\n\n  Weekday: %s" %(weather[0]['Temperature'],u'\N{DEGREE SIGN}',weather[0]['Description'],weather[0]['Humidity'],weather[0]['LocalTime'],weather[0]['DayOfWeek'],weather[0]['WeekDay']))

builder = Builder()
Gtk.main()