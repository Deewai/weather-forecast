#!/usr/bin/python3
import requests

class Api:
    locationEndpoint = 'https://places.cit.api.here.com/places/v1/discover/search?at=0,0&q='
    weatherEndpoint = 'https://weather.cit.api.here.com/weather/1.0/report.json?product=forecast_hourly&name='
    appCode = 'V2OQjQoYyjIiz2eh6lmWYQ'
    appId = '7YvYMVcPZ2lgQU8UTMi6'

    def getLocation(self, query):
        r = requests.get(self.locationEndpoint + query + '&app_code=' + self.appCode + '&app_id=' + self.appId)
        response = r.json()
        results = response['results']['items']
        locations = []
        for elem in results:
            locationItem = {
                'Title': elem['title'],
                'Vicinity': elem['vicinity'].replace('<br/>',', '),
                'Position': elem['position']
            }
            locations.append(locationItem)
        return locations

    def getWeather(self, location):
        r = requests.get(self.weatherEndpoint + location + '&app_code=' + self.appCode + '&app_id=' + self.appId)
        response = r.json()
        results = response['hourlyForecasts']['forecastLocation']['forecast']
        weather = []
        for elem in results:
            weatherItem = {
                'Daylight': elem['daylight'],
                'Description': elem['description'],
                'Temperature': elem['temperature'],
                'Humidity': elem['humidity'],
                'IconLink': elem['iconLink'],
                'DayOfWeek': elem['dayOfWeek'],
                'WeekDay': elem['weekday'],
                'Utctime': elem['utcTime'],
                'LocalTime': elem['localTime'],
                'LocalTimeFormat': elem['localTimeFormat']
            }
            weather.append(weatherItem)
        return weather