#!/usr/bin/python3
from tinydb import TinyDB, Query
import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk
class Db:
    def __init__(self):
        self.db = TinyDB('./db/db.json')
    def save(self, location, weather):
        self.db.upsert({'location': location, 'weather': weather}, (Query().location.Title == location['Title']) & (Query().location.Vicinity == location['Vicinity']))
        # self.db.insert({'location': location, 'weather': weather})

    def get(self, title, vicinity):
        data = self.db.search((Query().location.Title == title) & (Query().location.Vicinity == vicinity))
        if (len(data) > 0):
            return data[0]
        else:
            return None
    def getAll(self):
        all = self.db.search((Query().location.exists()) & (Query().weather.exists()))
        return all

    def rowTuple(self, row):
        if row['location'] is not None:
            return [row['location']['Title'],row['location']['Vicinity']]

    def storeModel(self):
        store = Gtk.ListStore(str, str)
        all = self.getAll()
        for elem in all:
            store.append(self.rowTuple(elem))
        return store